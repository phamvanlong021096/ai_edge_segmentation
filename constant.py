def rgb2bgr(p):
    return [p[2], p[1], p[0]]


NUM_CLASS = 20

IMAGE_SIZE = (320, 512)  # (h, w)

ORIGINAL_IMAGE_SIZE = (1216, 1936)

# CATEGORIES = [
#     ('others', rgb2bgr([81, 99, 0])),
#     ('car', rgb2bgr([0, 0, 255])),
#     ('pedestrian', rgb2bgr([255, 0, 0])),
#     ('lane', rgb2bgr([69, 47, 142])),
#     ('bus', rgb2bgr([193, 214, 0])),
#     ('truck', rgb2bgr([180, 0, 129])),
#     ('svehicle', rgb2bgr([255, 121, 166])),
#     ('motorbike', rgb2bgr([65, 166, 1])),
#     ('bicycle', rgb2bgr([208, 149, 1])),
#     ('signal', rgb2bgr([255, 255, 0])),
#     ('signs', rgb2bgr([255, 134, 0])),
#     ('sky', rgb2bgr([0, 152, 225])),
#     ('building', rgb2bgr([0, 203, 151])),
#     ('natural', rgb2bgr([85, 255, 50])),
#     ('wall', rgb2bgr([92, 136, 125])),
#     ('ground', rgb2bgr([136, 45, 66])),
#     ('sidewalk', rgb2bgr([0, 255, 255])),
#     ('roadshoulder', rgb2bgr([215, 0, 255])),
#     ('obstacle', rgb2bgr([180, 131, 135])),
#     ('own', rgb2bgr([86, 62, 67]))
# ]

CATEGORIES = [
    ('car', rgb2bgr([0, 0, 255])),
    ('pedestrian', rgb2bgr([255, 0, 0])),
    ('lane', rgb2bgr([69, 47, 142])),
    ('signal', rgb2bgr([255, 255, 0]))
]

CATEGORIES_CITISCAPES = [
    ('car', rgb2bgr([0, 0, 142])),
    ('pedestrian', rgb2bgr([220, 20, 60])),
    ('lane', rgb2bgr([128, 64, 128])),
    ('signal', rgb2bgr([250, 170, 30]))
]
