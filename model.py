import os
import cv2

import tensorflow as tf
import numpy as np

from utils import read_images, read_labels, shuffle_data
from constant import IMAGE_SIZE, CATEGORIES


class SEG_VGG16(object):
    def __init__(self, num_classes=2):
        self.num_classes = num_classes
        self.learning_rate = 0.0001
        self.vgg16_npy_path = os.getcwd() + '/weights/vgg16/vgg16.npy'
        self.weight_files = os.getcwd() + '/weights/seg_vgg16/seg_vgg16.ckpt'
        self.data_dict = np.load(self.vgg16_npy_path, encoding='latin1').item()

    def build(self):
        print("Building model...")
        self.X = tf.placeholder(tf.float32, shape=[None, IMAGE_SIZE[0], IMAGE_SIZE[1], 3], name='input_image')
        self.Y = tf.placeholder(tf.int32, shape=[None, IMAGE_SIZE[0], IMAGE_SIZE[1]], name='label')
        self.keep_prob = tf.placeholder(tf.float32, name='keep_probability')

        # Build vgg16
        self.conv1_1, self.relu1_1 = self._conv_layer(self.X, 3, 64, 'conv1_1')
        self.conv1_2, self.relu1_2 = self._conv_layer(self.relu1_1, 3, 64, 'conv1_2')
        self.pool1 = self._max_pool(self.relu1_2, 'pool1')

        self.conv2_1, self.relu2_1 = self._conv_layer(self.pool1, 3, 128, 'conv2_1')
        self.conv2_2, self.relu2_2 = self._conv_layer(self.relu2_1, 3, 128, 'conv2_2')
        self.pool2 = self._max_pool(self.relu2_2, 'pool2')

        self.conv3_1, self.relu3_1 = self._conv_layer(self.pool2, 3, 256, 'conv3_1')
        self.conv3_2, self.relu3_2 = self._conv_layer(self.relu3_1, 3, 256, 'conv3_2')
        self.conv3_3, self.relu3_3 = self._conv_layer(self.relu3_2, 3, 256, 'conv3_3')
        self.pool3 = self._max_pool(self.relu3_3, 'pool3')

        self.conv4_1, self.relu4_1 = self._conv_layer(self.pool3, 3, 512, 'conv4_1')
        self.conv4_2, self.relu4_2 = self._conv_layer(self.relu4_1, 3, 512, 'conv4_2')
        self.conv4_3, self.relu4_3 = self._conv_layer(self.relu4_2, 3, 512, 'conv4_3')
        self.pool4 = self._max_pool(self.relu4_3, 'pool4')

        self.conv5_1, self.relu5_1 = self._conv_layer(self.pool4, 3, 512, 'conv5_1')
        self.conv5_2, self.relu5_2 = self._conv_layer(self.relu5_1, 3, 512, 'conv5_2')
        self.conv5_3, self.relu5_3 = self._conv_layer(self.relu5_2, 3, 512, 'conv5_3')
        self.pool5 = self._max_pool(self.relu5_3, 'pool5')

        # Fully conv
        self.conv6, self.relu6 = self._conv_layer(self.pool5, 7, 4096, 'conv6')
        self.dropout6 = tf.nn.dropout(self.relu6, keep_prob=self.keep_prob)
        self.conv7, self.relu7 = self._conv_layer(self.dropout6, 1, 4096, 'conv7')
        self.dropout7 = tf.nn.dropout(self.relu7, keep_prob=self.keep_prob)

        # now to upscale to actual image size
        self.vgg3_reshaped = self._reshape(self.pool3, self.num_classes, 8, 'layer3_resize')
        self.vgg4_reshaped = self._reshape(self.pool4, self.num_classes, 16, 'layer4_resize')
        self.vgg7_reshaped = self._reshape(self.dropout7, self.num_classes, 32, 'layer7_resize')

        self.logits = tf.add(self.vgg3_reshaped, tf.add(2 * self.vgg4_reshaped, 4 * self.vgg7_reshaped))
        self.softmax = tf.nn.softmax(self.logits)  # default axis = -1
        self.classes = tf.argmax(self.softmax, axis=3)

        # get optimizer
        self.loss = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits, labels=self.Y))
        optimizer = tf.train.AdamOptimizer(self.learning_rate)
        self.opt = optimizer.minimize(self.loss)

        self.saver = tf.train.Saver()
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())

        print('Model initialized.')

    def _conv_layer(self, inputs, filter_size, num_filter, name):
        c = int(inputs.get_shape()[3])
        if name in self.data_dict:
            w = tf.Variable(self.data_dict[name][0], name=name + '_w')
            b = tf.Variable(self.data_dict[name][1], name=name + '_b')
        else:
            w = self._weight_variable([filter_size, filter_size, c, num_filter], name=name + '_w')
            b = self._bias_variable([num_filter], name=name + '_b')

        conv = tf.nn.conv2d(inputs, w, [1, 1, 1, 1], padding='SAME')
        conv_bias = tf.nn.bias_add(conv, b)
        conv_relu = tf.nn.relu(conv_bias)
        return conv_bias, conv_relu

    def _weight_variable(self, shape, name):
        return tf.Variable(tf.truncated_normal(shape, stddev=0.1), name)

    def _bias_variable(self, shape, name):
        return tf.Variable(tf.constant(0.1, shape=shape), name)

    def _max_pool(self, inputs, name):
        return tf.nn.max_pool(inputs, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name=name)

    def _reshape(self, inputs, num_channel, upscale_factor, name):
        c = int(inputs.get_shape()[3])
        w = self._weight_variable([1, 1, c, num_channel], name=name + '_w')
        b = self._bias_variable([num_channel], name=name + '_b')
        resized = tf.nn.conv2d(inputs, w, [1, 1, 1, 1], padding='VALID')
        resized = tf.nn.bias_add(resized, b)
        upsampled = self._upsample_layer(resized, num_channel, upscale_factor, name + '_upsampled')
        return upsampled

    def _upsample_layer(self, inputs, num_channel, upscale_factor, name):
        kernel_size = 2 * upscale_factor - upscale_factor % 2
        stride = upscale_factor
        strides = [1, stride, stride, 1]
        in_shape = tf.shape(inputs)

        h = in_shape[1] * stride
        w = in_shape[2] * stride
        new_shape = [in_shape[0], h, w, num_channel]
        output_shape = tf.stack(new_shape)

        filter_shape = [kernel_size, kernel_size, num_channel, num_channel]
        weights = self._get_bilinear_filter(filter_shape, upscale_factor, name + '_w')
        deconv = tf.nn.conv2d_transpose(inputs, weights, output_shape, strides=strides, padding='SAME')
        return deconv

    def _get_bilinear_filter(self, filter_shape, upscale_factor, name):
        # filter shape is [width, height, num_in_channel, num_out_channel]
        kernel_size = filter_shape[1]
        # Center location of the filter for which value is calculated
        if kernel_size % 2 == 1:
            center_location = upscale_factor - 1
        else:
            center_location = upscale_factor - 0.5

        bilinear = np.zeros([filter_shape[0], filter_shape[1]])
        for x in range(filter_shape[0]):
            for y in range(filter_shape[1]):
                # Interpolation calculation
                value = (1 - abs((x - center_location) / upscale_factor)) \
                        * (1 - abs((y - center_location) / upscale_factor))
                bilinear[x, y] = value
        weights = np.zeros(filter_shape)
        for i in range(filter_shape[2]):
            for j in range(filter_shape[3]):
                weights[:, :, i, j] = bilinear

        init = tf.constant_initializer(value=weights, dtype=tf.float32)
        bilinear_weights = tf.get_variable(name=name, initializer=init, shape=weights.shape)
        return bilinear_weights

    def train(self, train_image_paths, train_label_paths, valid_image_paths, valid_label_paths,
              num_epochs, bath_size, save_model_after=5):
        print("Start training...")
        num_train_image = len(train_image_paths)
        num_valid_image = len(valid_image_paths)
        num_batch_train = (num_train_image - 1) // bath_size + 1
        num_batch_valid = (num_valid_image - 1) // bath_size + 1

        for e in range(num_epochs):
            loss_train = 0
            loss_valid = 0

            # Shuffle data
            train_image_paths, train_label_paths = shuffle_data(train_image_paths, train_label_paths)

            # Train
            for i in range(num_batch_train):
                start_index = i * bath_size
                end_index = min(start_index + bath_size, num_train_image)
                batch_X = read_images(train_image_paths[start_index: end_index])
                batch_Y = read_labels(train_label_paths[start_index: end_index])
                _, loss = self.sess.run([self.opt, self.loss],
                                        feed_dict={self.X: batch_X, self.Y: batch_Y, self.keep_prob: 0.5})
                loss_train += loss
            loss_train /= num_batch_train

            # Valid
            for i in range(num_batch_valid):
                start_index = i * bath_size
                end_index = min(start_index + bath_size, num_valid_image)
                batch_X = read_images(valid_image_paths[start_index: end_index])
                batch_Y = read_labels(valid_label_paths[start_index: end_index])
                loss = self.sess.run(self.loss,
                                     feed_dict={self.X: batch_X, self.Y: batch_Y, self.keep_prob: 1.0})
                loss_valid += loss
            loss_valid /= num_batch_valid

            print("Step: ", e)
            print("     Loss train: ", loss_train)
            print("     Loss valid: ", loss_valid)

            if e % save_model_after == 0:
                self._save_model()

        self._save_model()
        print("Training done !")

    def _save_model(self):
        self.saver.save(self.sess, self.weight_files)

    def restore_weight(self):
        self.saver.restore(self.sess, self.weight_files)
        print("Model restored !")

    def predict(self, image_path):
        img = cv2.imread(image_path)
        img = cv2.resize(img, (IMAGE_SIZE[1], IMAGE_SIZE[0]), interpolation=cv2.INTER_NEAREST)
        img = np.asarray([img.astype(np.float32)])
        classes = self.sess.run(self.classes,
                                feed_dict={self.X: img, self.keep_prob: 1.0})
        classes = classes[0]
        res = np.zeros([IMAGE_SIZE[0], IMAGE_SIZE[1], 3], dtype=np.int32)
        for i, c in enumerate(CATEGORIES):
            x, y = np.where(classes == i + 1)
            if len(x) > 0 and len(y) > 0:
                res[x, y] = c[1]

        return res

    def test(self):
        print(self.data_dict['conv5_3'][0])
        print(self.data_dict['conv5_3'][1])
