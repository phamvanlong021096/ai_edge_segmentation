from utils import shuffle_data

a = ["aa", "bb", "cc", "dd"]
b = [1, 2, 3, 4]
a, b = shuffle_data(a, b)
print(a)
print(b)
