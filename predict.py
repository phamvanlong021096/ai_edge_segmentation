import os
import cv2

from model import SEG_VGG16
from constant import ORIGINAL_IMAGE_SIZE


def main():
    img_dir = "D:\\data\\ai_edge_segmentation\\seg_test_images"
    out_dir = "D:\\data\\ai_edge_segmentation\\test_predict"

    # Build model
    model = SEG_VGG16(num_classes=5)
    model.build()
    model.restore_weight()

    for f in os.listdir(img_dir):
        print("Predict image: ", f)
        r = model.predict(os.path.join(img_dir, f))
        # r = r / 255.0
        image = cv2.resize(r, (ORIGINAL_IMAGE_SIZE[1], ORIGINAL_IMAGE_SIZE[0]), interpolation=cv2.INTER_NEAREST)

        cv2.imwrite(os.path.join(out_dir, os.path.splitext(f)[0] + '.png'), image)


if __name__ == '__main__':
    main()
