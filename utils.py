import cv2
import random
import numpy as np


def read_images(paths):
    images = []
    for p in paths:
        image = cv2.imread(p)
        images.append(image.astype(np.float32))
    return np.asarray(images)


def read_labels(paths):
    labels = []
    for p in paths:
        label = np.loadtxt(p, dtype=np.int32)
        labels.append(label)
    return np.asarray(labels)


def shuffle_data(image_paths, label_paths):
    array = []
    n = len(image_paths)
    for i in range(n):
        array.append((image_paths[i], label_paths[i]))
    random.shuffle(array)
    image_paths = [a[0] for a in array]
    label_paths = [a[1] for a in array]
    return image_paths, label_paths
