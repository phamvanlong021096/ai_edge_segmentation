import os
import cv2
import numpy as np
from constant import IMAGE_SIZE, CATEGORIES_CITISCAPES


def get_image_files(root):
    image_files = []
    dirs = [os.path.join(root, d) for d in os.listdir(root)]
    for d in dirs:
        for f in os.listdir(d):
            image_files.append(os.path.join(d, f))

    return image_files


def get_label_files(root):
    label_files = []
    dirs = [os.path.join(root, d) for d in os.listdir(root)]
    for d in dirs:
        for f in os.listdir(d):
            if 'color' in f:
                label_files.append(os.path.join(d, f))

    return label_files


def copy_image(from_path, to_path):
    img = cv2.imread(from_path)
    img = cv2.resize(img, (IMAGE_SIZE[1], IMAGE_SIZE[0]), interpolation=cv2.INTER_NEAREST)
    cv2.imwrite(to_path, img)


def convert_label(from_path, to_path):
    img = cv2.imread(from_path)
    img = cv2.resize(img, (IMAGE_SIZE[1], IMAGE_SIZE[0]), interpolation=cv2.INTER_NEAREST)

    label = np.zeros([IMAGE_SIZE[0], IMAGE_SIZE[1]], dtype=np.int32)
    for i, c in enumerate(CATEGORIES_CITISCAPES):
        lb = np.all(img == c[1], axis=2)
        lb = lb.astype(np.int32)
        label = label + lb * (i + 1)

    # save label to file
    np.savetxt(to_path, label)


def main():
    image_train_root = "D:\\data\\cityscapes\\leftImg8bit\\train"
    image_val_root = "D:\\data\\cityscapes\\leftImg8bit\\val"

    label_train_root = "D:\\data\\cityscapes\\gtFine\\train"
    label_val_root = "D:\\data\\cityscapes\\gtFine\\val"

    root = os.getcwd() + "/data"
    train_image_dir = root + "/train_image"
    train_label_dir = root + "/train_label"
    valid_image_dir = root + "/valid_image"
    valid_label_dir = root + "/valid_label"

    # create data dir
    if not os.path.exists(root):
        os.mkdir(root)
    if not os.path.exists(train_image_dir):
        os.mkdir(train_image_dir)
    if not os.path.exists(train_label_dir):
        os.mkdir(train_label_dir)
    if not os.path.exists(valid_image_dir):
        os.mkdir(valid_image_dir)
    if not os.path.exists(valid_label_dir):
        os.mkdir(valid_label_dir)

    # get all image files
    train_image_files = get_image_files(image_train_root)
    val_image_files = get_image_files(image_val_root)

    train_label_files = get_label_files(label_train_root)
    val_label_files = get_label_files(label_val_root)

    # copy image to data directory
    print("Coppy train images to data directory...")
    for f in train_image_files:
        f_name = os.path.basename(f)
        copy_image(f, os.path.join(train_image_dir, f_name))

    print("Coppy val images to data directory...")
    for f in val_image_files:
        f_name = os.path.basename(f)
        copy_image(f, os.path.join(valid_image_dir, f_name))

    print("Convert train label...")
    for f in train_label_files:
        f_name = os.path.basename(f)
        label_file_txt = os.path.splitext(f_name)[0] + '.txt'
        convert_label(f, os.path.join(train_label_dir, label_file_txt))

    print("Convert val label...")
    for f in val_label_files:
        f_name = os.path.basename(f)
        label_file_txt = os.path.splitext(f_name)[0] + '.txt'
        convert_label(f, os.path.join(valid_label_dir, label_file_txt))

    print("Done !")


if __name__ == '__main__':
    main()
