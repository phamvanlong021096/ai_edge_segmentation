import os
import cv2
import numpy as np
from constant import CATEGORIES, IMAGE_SIZE


def copy_image(from_path, to_path):
    img = cv2.imread(from_path)
    img = cv2.resize(img, (IMAGE_SIZE[1], IMAGE_SIZE[0]), interpolation=cv2.INTER_NEAREST)
    cv2.imwrite(to_path, img)


def convert_label(from_path, to_path):
    img = cv2.imread(from_path)
    img = cv2.resize(img, (IMAGE_SIZE[1], IMAGE_SIZE[0]), interpolation=cv2.INTER_NEAREST)

    label = np.zeros([IMAGE_SIZE[0], IMAGE_SIZE[1]], dtype=np.int32)
    for i, c in enumerate(CATEGORIES):
        lb = np.all(img == c[1], axis=2)
        lb = lb.astype(np.int32)
        label = label + lb * (i + 1)

    # save label to file
    np.savetxt(to_path, label)


def main():
    image_dir = "D:\\data\\ai_edge_segmentation\\seg_train_images"
    label_dir = "D:\\data\\ai_edge_segmentation\\seg_train_annotations"
    valid_fraction = 0.0

    root = os.getcwd() + "/data"
    train_image_dir = root + "/train_image"
    train_label_dir = root + "/train_label"
    valid_image_dir = root + "/valid_image"
    valid_label_dir = root + "/valid_label"

    # create data dir
    if not os.path.exists(root):
        os.mkdir(root)
    if not os.path.exists(train_image_dir):
        os.mkdir(train_image_dir)
    if not os.path.exists(train_label_dir):
        os.mkdir(train_label_dir)
    if not os.path.exists(valid_image_dir):
        os.mkdir(valid_image_dir)
    if not os.path.exists(valid_label_dir):
        os.mkdir(valid_label_dir)

    # split in to train and valid
    image_files = os.listdir(image_dir)
    label_files = os.listdir(label_dir)
    label_files = [f for f in label_files if '.png' in f]

    num_images = len(image_files)
    t = int(valid_fraction * num_images)
    for i in range(num_images):
        image_file = image_files[i]
        label_file = label_files[i]
        label_file_txt = os.path.splitext(label_file)[0] + '.txt'
        print("Preprocess image: " + image_file)
        if i < t:  # valid
            copy_image(os.path.join(image_dir, image_file), os.path.join(valid_image_dir, image_file))
            convert_label(os.path.join(label_dir, label_file), os.path.join(valid_label_dir, label_file_txt))
        else:  # train
            copy_image(os.path.join(image_dir, image_file), os.path.join(train_image_dir, image_file))
            convert_label(os.path.join(label_dir, label_file), os.path.join(train_label_dir, label_file_txt))

    print("Preprocess done !")


if __name__ == '__main__':
    main()
