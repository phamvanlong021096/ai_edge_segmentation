import os
from model import SEG_VGG16


def main():
    # read data paths
    root = os.getcwd() + "/data"
    train_image_dir = root + "/train_image"
    train_label_dir = root + "/train_label"
    valid_image_dir = root + "/valid_image"
    valid_label_dir = root + "/valid_label"

    train_image_paths = [os.path.join(train_image_dir, f) for f in os.listdir(train_image_dir)]
    train_label_paths = [os.path.join(train_label_dir, f) for f in os.listdir(train_label_dir)]
    valid_image_paths = [os.path.join(valid_image_dir, f) for f in os.listdir(valid_image_dir)]
    valid_label_paths = [os.path.join(valid_label_dir, f) for f in os.listdir(valid_label_dir)]

    train_from_scratch = False

    # train model
    model = SEG_VGG16(num_classes=5)
    model.build()
    if not train_from_scratch:
        model.restore_weight()

    model.train(train_image_paths, train_label_paths, valid_image_paths, valid_label_paths, num_epochs=5, bath_size=10)


if __name__ == '__main__':
    main()
